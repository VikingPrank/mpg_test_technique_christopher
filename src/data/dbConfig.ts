import { Cluster, connect } from "couchbase";

const clusterConnStr = "couchbase://localhost";
const username = process.env.COUCHBASE_USERNAME || "admin";
const password = process.env.COUCHBASE_PASSWORD || "monpetitgazon";

export const cluster = async () => {
  const cluster: Cluster = await connect(clusterConnStr, {
    username: username,
    password: password,
  });
  return cluster;
};
