import express from "express";
import leagueController from "./controllers/leagueController";
import setupSwagger from "./swagger";

async function main() {
  const app = express();
  const port = 3000;
  setupSwagger(app);
  app.use(express.json());

  app.get("/", leagueController.getLeagues);
  app.get("/users-by-league/:leagueId", leagueController.getUsersByLeague);
  app.post("/league", leagueController.createLeague);
  app.patch("/team/:teamId", leagueController.updateTeam);

  app.listen(port, () => {
    console.log(`MPG API on port ${port}`);
  });
}

main();

/**
 * TESTING
 * curl -X GET http://localhost:3000/
 * curl -X GET http://localhost:3000/users-by-league/mpg_league_1
 * curl -X PATCH -H "Content-Type: application/json" -d '{"name": "New Team Name"}' http://localhost:3000/team/mpg_team_1_3
 * curl -X POST -H "Content-Type: application/json" -d '{"id": "123", "name": "Example League", "description": "Testing League", "adminId": "admin123"}' http://localhost:3000/league
 */
