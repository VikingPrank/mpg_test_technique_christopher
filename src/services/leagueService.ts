import { Bucket, Collection, QueryResult } from "couchbase";
import { cluster } from "../data/dbConfig";
import couchbase from "couchbase";

async function getAllLeagues() {
  const clus = await cluster();
  const query = `SELECT * FROM mpg`;
  try {
    const result: QueryResult = await clus.query(query);
    const rows = result.rows.map((row) => row);
    return rows;
  } catch (error) {
    console.error(error);
    throw new Error("An error occurred while retrieving leagues.");
  }
}

async function getUsersByLeague(leagueId: string) {
  const clus = await cluster();
  const queryLeagues = `SELECT * FROM mpg WHERE type = 'mpg_league' AND id = '${leagueId}'`;
  const teamsOfLeague = await clus
    .query(queryLeagues)
    .then((result: any) => {
      const leagues = [];
      for (const league of result.rows) {
        leagues.push(league);
      }
      const teams = leagues[0]?.mpg?.usersTeams;
      return teams;
    })
    .catch((error: Error) => {
      console.error(error);
      throw new Error("An error occurred while retrieving leagues.");
    });

  if (!teamsOfLeague) {
    throw new Error(`No teams founds in league ${leagueId}`);
  }

  const teamsArray = Object.entries(teamsOfLeague);

  const usersOfTeams = await Promise.all(
    teamsArray.map(async ([userId, teamId]) => {
      const query = `SELECT * FROM mpg WHERE type = 'user' AND id = '${userId}'`;
      const usersOfTeam = await clus
        .query(query)
        .then((result: any) => {
          const users = [];
          for (const user of result.rows) {
            users.push(user);
          }
          return users;
        })
        .catch((error: Error) => {
          throw new Error("An error occurred while users of the league.");
        });
      return usersOfTeam;
    })
  );
  const result = {
    users: usersOfTeams.map(([team]) => ({
      name: team.mpg.name,
    })),
  };
  return result;
}

async function createLeague(
  id: string,
  name: string,
  description: string,
  adminId: string
) {
  const clus = await cluster();
  const query = `SELECT * FROM mpg WHERE type = 'mpg_league' AND id = '${id}'`;
  const leagues = await clus.query(query).then((result: any) => {
    const leagues = [];
    for (const league of result.rows) {
      leagues.push(league);
    }
    return leagues;
  });

  if (leagues.length > 0) {
    throw new Error(`League ${id} already exists`);
  }

  // create league:
  const league = {
    adminId,
    description,
    id,
    name,
    type: "mpg_league",
    usersTeams: {},
  };
  const bucket: Bucket = clus.bucket("mpg");
  const collection = bucket.defaultCollection();
  await collection.insert(id, league);

  return league;
}

async function updateTeam(teamId: string, name: string) {
  const clus = await cluster();
  const bucket: Bucket = clus.bucket("mpg");
  const collection: Collection = bucket.defaultCollection();
  try {
    await collection.mutateIn(teamId, [
      couchbase.MutateInSpec.upsert("name", name),
    ]);
  } catch (error) {
    console.error("mutateIn error:", error);
  }

  return { teamId, name };
}

export default {
  getAllLeagues,
  getUsersByLeague,
  createLeague,
  updateTeam,
};
