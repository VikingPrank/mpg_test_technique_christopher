import { Request, Response } from "express";
import leagueService from "../services/leagueService";

/**
 * @swagger
 * /:
 *   get:
 *     summary: Get all leagues
 *     responses:
 *       200:
 *         description: Returns an array of leagues
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: string
 *                   name:
 *                     type: string
 *                   description:
 *                     type: string
 *                   adminId:
 *                     type: string
 */
async function getLeagues(req: Request, res: Response) {
  let leagues;
  try {
    leagues = await leagueService.getAllLeagues();
  } catch (error: any) {
    console.error("getLeagues error", error);
    return res.status(500).send(error.message);
  }
  return res.send(leagues);
}

/**
 * @swagger
 * /users-by-league/{leagueId}:
 *   get:
 *     summary: Get users by league ID
 *     parameters:
 *       - in: path
 *         name: leagueId
 *         required: true
 *         schema:
 *           type: string
 *         description: ID of the league
 *     responses:
 *       200:
 *         description: Returns an array of users in the league
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 users:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       name:
 *                         type: string
 *       400:
 *         description: Bad Request - leagueId is required
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 */
async function getUsersByLeague(req: Request, res: Response) {
  const leagueId = req?.params?.leagueId;
  if (!leagueId) {
    res.status(400).send("leagueId is required");
    return;
  }
  let users;
  try {
    users = await leagueService.getUsersByLeague(leagueId);
  } catch (error: any) {
    console.error("getUsersByLeague error", error);
    return res.status(500).send(error.message);
  }
  return res.send(users);
}

/**
 * @swagger
 * /league:
 *   post:
 *     summary: Create a new league
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               id:
 *                 type: string
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *               adminId:
 *                 type: string
 *     responses:
 *       200:
 *         description: League created successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: string
 *                 name:
 *                   type: string
 *                 description:
 *                   type: string
 *                 adminId:
 *                   type: string
 *       400:
 *         description: Bad Request - id, name, description, adminId are required
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 */
async function createLeague(req: Request, res: Response) {
  const { id, name, description, adminId } = req.body;
  if (!id || !name || !description || !adminId) {
    res.status(400).send("id, name, description, adminId are required");
    return;
  }
  let createdLeague;
  try {
    createdLeague = await leagueService.createLeague(
      id,
      name,
      description,
      adminId
    );
  } catch (error: any) {
    console.error("createLeague error", error);
    return res.status(400).send(error?.message);
  }
  return res.send(createdLeague);
}

/**
 * @swagger
 * /team/{teamId}:
 *   patch:
 *     summary: Update team by ID
 *     parameters:
 *       - in: path
 *         name: teamId
 *         required: true
 *         schema:
 *           type: string
 *         description: ID of the team
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *     responses:
 *       200:
 *         description: Team updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 teamId:
 *                   type: string
 *                 name:
 *                   type: string
 *       400:
 *         description: Bad Request - teamId and name are required
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 error:
 *                   type: string
 */
async function updateTeam(req: Request, res: Response) {
  const { teamId } = req.params;
  const { name } = req.body;
  console.log("teamId", teamId);
  console.log("name", name);
  if (!teamId || !name) {
    res.status(400).send("teamId and name are required");
    return;
  }
  let updatedTeam;
  try {
    updatedTeam = await leagueService.updateTeam(teamId, name);
  } catch (error: any) {
    console.error("updateTeam error", error);
    return res.status(400).send(error?.message);
  }
  return res.send(updatedTeam);
}

export default {
  getLeagues,
  getUsersByLeague,
  createLeague,
  updateTeam,
};
