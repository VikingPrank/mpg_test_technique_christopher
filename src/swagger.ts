import swaggerJSDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

const swaggerOptions = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "MPG API",
      version: "1.0.0",
      description: "API documentation for MPG",
    },
    servers: [
      {
        url: "http://localhost:3000", // Update the URL if needed
        description: "Development server",
      },
    ],
  },
  apis: ["./src/controllers/leagueController.ts"], // Update with your controller file path(s)
};

const swaggerSpec = swaggerJSDoc(swaggerOptions);

export default function setupSwagger(app: any) {
  app.use("/doc", swaggerUi.serve, swaggerUi.setup(swaggerSpec));
}
